# Next Steps

## Congrats!

You've successfully completed the Practical Guide to Cloud - _good job!_

Otherwise, I sincerely hope that you've found this workshop useful, educational, and interesting.

## Feedback

To receive a badge for the completion of the course and workshop, make sure to fill out the feedback form located on the [Hartree Training Portal](https://hartreetraining.stfc.ac.uk/moodle/local/hartree/index.php). Your feedback is vital to the improvement of this course and the workshop!


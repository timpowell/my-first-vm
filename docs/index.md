# Introduction

Welcome to My First VM, a tutorial that will take you through all the steps required to deploy a local and remote Virtual Machine (VM). You do not need any prior experience for this tutorial, however basic familiarity with Linux command line will be beneficial.

In this tutorial, you will create a virtual Raspberry Pi in [Virtual Box](https://www.virtualbox.org/) (if you have not installed this, please do so now) and deploy a containerised application on your local machine. You will then be given a whistle stop tour of the Azure Portal, where you shall then have a chance to create a remote VM and deploy the same containerised application on the cloud.

!!! info "Make It Your Own!"
    Anywhere you see < > indicates that you will need to add your own custom information. For example, we shall be naming our virtual machines: `myfirstvm-<INITIALS>`. As my name is Tim Powell, my virtual machine will be called `myfirstvm-TP`.

    Tempting as it might be, you'll get a lot more out of this tutorial if you go through all the commands and code modifications yourself, rather than just skipping ahead.

    I'd also suggest typing out the code and commands yourself instead of simply copy & pasting - by typing everything out, you'll do two things:

    1. Better process what you're typing
    2. Probably make typos - this is great! Making mistakes and fixing them is the best way to learn.

## :speech_balloon: Giving feedback and getting help

If you get stuck or encounter a problem, please unmute yourself and ask us your question.

You can also comment on any of the tutorial sections using Disqus - any and all feedback, corrections or questions are very welcome!

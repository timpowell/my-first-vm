# Troubleshooting

## VirtualBox

### Linux

If you are using Linux, VirtualBox is not the best software for creating and using Virtual Machines. [QEMU](https://www.qemu.org/) is a good alternative option. The instructions will be slightly different than VirtualBox, please ask if you get stuck.

### Windows

If you are running on a Windows machine, you may encounter the following issue:

```bash
Call to NEMR0InitVMPart2 failed: VERR_NEM_INIT_FAILED (VERR_NEM_VM_CREATE_FAILED)
```

The source of this issue a the Windows hypervisor Hyper-V running in the background and interfering with VirtualBox's hypervisor. Unfortunately, you require admin privileges to stop Hyper-V. If you have admin privileges please follow these instructions:

1. Open a command prompt (CMD) as administrator
2. Type `bcdedit /set hypervisorlaunchtype off`
3. Reboot your laptop

If you do not have admin privileges, continue on to the next section. You will get a chance to deploy VM's in Azure later on in the course.

### Mac

If you are running VirtualBox on mac, you may encounter the following issue:

```bash
Kernel driver not installed (rc=-1908)
```

To solve this, open 'System Preferences' -> 'Security & Privacy'. Under the “General” tab, there should be text near the bottom that says, “System Software from Developer ‘Oracle America, Inc.’ Was Blocked from Loading.” Click the “Allow” button.

**Note**: This option is available only for roughly 30 minutes after a fresh install of VirtualBox. If this message does not appear, uninstall VirtualBox by opening your “Applications” folder and then dragging the VirtualBox app to the Trash. Remove any leftover files, reinstall a fresh copy of VirtualBox, and immediately open the Security & Privacy menu to see this option.
